package com.ohgiraffers.section02.looping_and_branching.level01.nomal;

import java.util.Scanner;

public class Application2 {

	public static void main(String[] args) {
		
		/* 1부터 입력받은 정수까지의 짝수의 합을 구하세요
		 * 
		 * -- 입력 예시 --
		 * 정수를 입력하세요 : 10
		 * 
		 * -- 출력 예시 --
		 * 1부터 10까지 짝수의 합 : 30
		 * */
		
		Scanner sc = new Scanner(System.in);
		System.out.print("정수를 입력하세요 : ");
		int num = sc.nextInt();
		if(num<1) {
			System.out.print("프로그램 종료");
			System.exit(0);
		}
		int sum = 0;
		for(int i=1;i<=num;i++) {
			if(i%2==0) {
				sum += i;
			}
		}
		System.out.println(sum);
	}
}
