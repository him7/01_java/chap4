package com.ohgiraffers.section01.conditional.level03.hard;

import java.util.Scanner;

public class Application3 {

	public static void main(String[] args) {
		
		/* 아이디, 비밀번호를 정해두고 로그인 기능을 작성하세요
		 * 아이디, 비밀번호를 입력받아서 정해놓은 아이디와 비밀번호가 동일하여
		 * 로그인 성공 시 "로그인 성공", 아이디가 틀렸을 시 "아이디가 틀렸습니다."
		 * 비밀번호가 틀렸을 시 "비밀번호가 틀렸습니다."를 출력하세요
		 * */
		
		String id = "user01";
		String pwd = "pass01";
		
		Scanner sc = new Scanner(System.in);
		System.out.print("아이디 : ");
		String userId = sc.nextLine();
		System.out.print("비밀번호 : ");
		String userPwd = sc.nextLine();
		
		if(id.equals(userId) && pwd.equals(userPwd)) {
			System.out.println("로그인 성공");
		} else if(id.equals(userId)) {
			System.out.println("비밀번호가 틀렸습니다.");
		} else if(pwd.equals(userPwd)) {
			System.out.println("아이디가 틀렸습니다.");
		}
		
		System.out.println("=====================================");
		if(id.equals(userId)) {
			if(pwd.equals(userPwd)) {
				System.out.println("로그인 성공");
			} else {
				System.out.println("비밀번호가 틀렸습니다.");
			}
		} else {
			System.out.println("아이디가 틀렸습니다.");
		}
		
	}
}
