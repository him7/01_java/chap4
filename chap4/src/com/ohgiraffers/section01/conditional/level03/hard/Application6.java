package com.ohgiraffers.section01.conditional.level03.hard;

import java.util.Scanner;

public class Application6 {

	public static void main(String[] args) {
		
		/*
		 * 임의의 숫자 하나를 입력받아서 봄(3~5월), 여름(6~8월), 가을(9~11월),겨울(12, 1,2월)
		 * 의 각월에 해당하는 숫자를 찾아서 어느 계절인지를 출력하시오
		 * 단, 1 ~ 12월에 대한 값이 아닌경우 "잘못입력하셨습니다"라고 출력한다.
		 * 
		 * ex) 
		 * 1 ~ 12 사이 정수 입력 : 3
		 *    3월은 봄입니다.
		 *    
		 * 1 ~ 12 사이 정수 입력 : 13
		 *     잘못입력하셨습니다.
		 *    
		 * 
		 * */
		
		Scanner sc = new Scanner(System.in);
		System.out.print("1 ~ 12 사이 정수 입력 : ");
		int month = sc.nextInt();
		if(month>=3 && month<=5) {
			System.out.println(month+"월은 봄입니다.");
		} else if(month>=6 && month<=8) {
			System.out.println(month+"월은 여름입니다.");
		} else if(month>=9 && month<=11) {
			System.out.println(month+"월은 가을입니다.");
		} else if(month==12 || month>=1 && month<=2) {
			System.out.println(month+"월은 겨울입니다.");
		} else {
			System.out.println("잘못입력하셨습니다.");
		}
	}
}
