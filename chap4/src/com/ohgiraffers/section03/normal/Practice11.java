package com.ohgiraffers.section03.normal;

import java.util.Scanner;

public class Practice11 {

	public static void main(String[] args) {
		/*
		두 개의 주사위가 만들어낼 수 있는 모든 경우의 수를 랜덤으로 정하고
		랜덤으로 정해진 두 주사위 눈의 합이 입력된 수와 같은 경우 “맞췄습니다“ 출력,
		입력 값과 다르면 “틀렸습니다“ 출력하여 맞출 때까지 반복
		값을 맞추면 “계속 하시겠습니까? (y/n) : “가 출력되고 
		‘y’ 또는 ‘Y’ 입력 시 새로운 랜덤 수가 정해지고 처음부터 다시 시작, ‘n’ 또는 ‘N’ 입력 시 종료

		ex.
		주사위 두개의 합을 맞춰보세요(1~12입력) : 5
		정답입니다.
		주사위의 합 : 5
		계속 하시겠습니까?(y/n) : y
		주사위 두개의 합을 맞춰보세요(1~12입력) : 12
		틀렸습니다.
		주사위 두개의 합을 맞춰보세요(1~12입력) : 8
		틀렸습니다.
		주사위 두개의 합을 맞춰보세요(1~12입력) : 9
		정답입니다.
		주사위의 합 : 9
		계속 하시겠습니까?(y/n) : n
		종료합니다.
		*/
		
		int dice1 = (int)(Math.random()*6)+1;
		int dice2 = (int)(Math.random()*6)+1;
		int dSum = dice1+dice2;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("주사위 두개의 합을 맞춰보세요(1~12입력) : ");
			int num = sc.nextInt();
			if(dSum == num) {
				System.out.print(
						"정답입니다.\r\n" +
						"주사위의 합 : "+dSum+"\r\n" +
						"계속 하시겠습니까?(y/n) : "
						);
				while(true) {
					char flag = sc.next().charAt(0);
					if(flag == 'n' || flag == 'N') {
						System.out.println("종료합니다.");
						return;
					} else if(flag == 'y' || flag == 'Y'){
						dSum = (int)(Math.random()*12)+1;
						break;
					} else {
						System.out.println("다시 입력해주세요.");
						System.out.print("계속 하시겠습니까?(y/n) : ");
					}
				}
			} else {
				System.out.println("틀렸습니다.");
			}
		}
		
	}
}
